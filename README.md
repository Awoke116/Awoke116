# Hello there; welcome 👋🏾

 [![Linkedin Badge](https://img.shields.io/badge/-victoryndubuisi-blue?style=for-the-badge&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/victory-ndubuisi-319312226)](https://www.linkedin.com/in/victory-ndubuisi-319312226)

I'm an Editor, Proofreader, and an upcoming Project Manager, who currently works as an Editor at [newline](https://newline.co). I'm passionate about editing grammar, sharing knowledge, and others. I edit content at newline, speak at some meetups/conferences, and build communities...

**Here's a quick summary about me**:

- 😊 Pronouns: He/him
- 💡 Fun fact: I have edited about 20 books and courses for newline. Each is about 20,000 to 50,000 words.
- 🌱 I’m currently learning Project Management
- 😊 I’m looking for help with open source projects, hackathons, internships, and entry-level opportunities.
- 💼 Job interests: Editor/Proofreader, Project Manager
- 📫 You can [view my resume](#) and contact me by emailing ndubuisivictory737@gmail.com.

---

| <img align="center" src="https://gitlab-readme-stats.vercel.app/api?username=Awoke116&show_icons=true&include_all_commits=true&hide_border=true" alt="Victory's GitLab stats" /> | <img align="center" src="https://gitlab-readme-stats.vercel.app/api/top-langs/?username=Awoke116&langs_count=8&layout=compact&hide_border=true" alt="Victory's GitLab stats" /> |
| ------------- | ------------- |
